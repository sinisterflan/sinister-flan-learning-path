from django.shortcuts import render, redirect
# from django.contrib.auth.forms import UserCreationForm
from .admin import UserCreationForm
from django.contrib.auth import get_user_model
User = get_user_model()

def signup_view(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid:
            print('form is valid')
            form.save()
            return redirect('/')
    else:
        form = UserCreationForm()
    return render(
        request,
        'accounts/signup.html',
        {'form': form}
    )